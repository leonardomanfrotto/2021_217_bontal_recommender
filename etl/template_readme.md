This folder contains all the etl code

```
etl
+-- stg
    What: Staging layer
    Who: DAV + DS
+-- trn
    What: Transformation layer
    Who: DAV + DS
+-- mdl
    What: Model layer
    Who: DAV + DS
+-- prs
    What: Presentation layer
    Who: DAV + DS
+-- utils
    What: Utility scripts strictly needed for the deploy. Everything else goes
          in the package if possible.
    Who: DAV + DS
```