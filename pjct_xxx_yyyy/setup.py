# License (?)

import os
import re
from setuptools import setup, find_packages
from pathlib import Path

MOD_NAME = 'pjct_xxx_yyyy'
MOD_AUTHOR = "Dream Team @ Ammagamma"
MOD_AUTHOR_EMAIL = 'daniele.bigoni@ammagamma.com'
MOD_DESCRIPTION = 'Ammagamma Project Template'
MOD_ZIP_SAFE = True

# This aggregates all the requirements across the project
req_path = Path('requirements.txt')
with open(req_path, 'r') as istr:
    install_requires = istr.readlines()

# Get version string
local_path = os.path.split(os.path.realpath(__file__))[0]
version_file = os.path.join(
    local_path,
    MOD_NAME + '/_version.py'
)
version_strline = open(version_file).read()
VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
mo = re.search(VSRE, version_strline, re.M)
if mo:
    version = mo.group(1)
else:
    raise RuntimeError(
        "Unable to find version string in %s." % (version_file,)
    )

scripts_path = Path('scripts')
scripts = []
for path in scripts_path.glob('**/*.py'):
    scripts.append(str(path))

setup(
    name=MOD_NAME,
    version=version,
    author=MOD_AUTHOR,
    author_email=MOD_AUTHOR_EMAIL,
    license='LICENSE',
    description=MOD_DESCRIPTION,
    long_description=open('README.rst').read(),
    install_requires=install_requires,
    zip_safe=MOD_ZIP_SAFE,
    packages=[MOD_NAME],
    include_package_data=True,
    scripts=scripts
)
