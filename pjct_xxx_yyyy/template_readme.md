This is the main module of the project where both core functionalities and 
the etl is stored.
The file setup.py allows the packaging and installation of the module
(all the files `template_readme.md` are ignored by the packaging script)

```
pjct_xxx_yyyy
+-- pjct_xxx_yyyy
    What: All the package functions/classes.
    Who: DS, maybe something DAV during deploy
+-- queries
    What: Sql queries for the project.
    Who: DS, maybe something DAV during deploy
+-- scripts
    What: all the scripts.
    Who: DS + DAV
```