This is the main directory of the project. 
This folder is versioned with git, but some of components are ignored.
The file structure should adhere as much as possible to the following.

```
project
+-- backend
    What: all the files concerning the backend.
    Who: DAV
+-- data
    What: all the memory consuming data got from the client and 
          all the output data our executable produce.
    Who: DS + DAV
    Notes: Not versioned
+-- frontend
    What: all the files concerning the frontend.
    Who: DAV
+-- configs
    What: All config files
    Who: DS + DAV
+-- docker
    What: All files needed for the configuration and creation of docker images,
          creation of dockers and launch of dockers
    Who: DAV for deploy, DS for early testing
+-- docker-images
    What: all the docker images created for the deploy or for testing
    Who: Mostly DAV, sometimes DS
    Notes: Not versioned
+-- extra-packages
    What: packages developed externally and that need to be included for the deploy.
          These may include particular versions of bamboo, troisi, encoreds etc.
          or even packages not easily available using pip.
    Note: Only the compressed packages must be versioned (see .gitignore)
    Who: DAV + DS
+-- notebooks
    What: All the messy notebooks DS like a lot!
    Who: DS
+-- pjct_xxx_yyyy
    What: The project package, where <pjct> is the short name of the project,
          <xxx_yyyy> are the number and year of the project.
          The idea of using a package for the core functions is that this makes 
          it available to the notebooks as well as for the deploy.
    Who: DS + DAV
```